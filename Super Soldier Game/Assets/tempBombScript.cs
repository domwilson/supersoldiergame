﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class tempBombScript : MonoBehaviour
{
    bool followBomb = false;

    bool loadToGame = false;
    float loadTime = 0f;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > 16 && !followBomb)
        {
            this.GetComponent<SpriteRenderer>().enabled = true;
            this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.down, ForceMode2D.Impulse);
            GameObject.Find("CM vcam3").GetComponent<Cinemachine.CinemachineVirtualCamera>().enabled = true;
            followBomb = true;
        }

        if(loadToGame && Time.time - loadTime > 1)
        {
            SceneManager.LoadScene(1);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject.Find("CM vcam4").GetComponent<Cinemachine.CinemachineVirtualCamera>().enabled = true;
        loadTime = Time.time;
        loadToGame = true;
    }
}
