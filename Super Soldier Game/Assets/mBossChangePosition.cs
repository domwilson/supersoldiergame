﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mBossChangePosition : MonoBehaviour
{

    public GameObject triggerEnemy;
    bool playerFromSewer = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (triggerEnemy == null)
        {
            playerFromSewer = true;
        }

        if(playerFromSewer)
        {
            this.transform.position = this.transform.position + Vector3.right * 20;
            Destroy(this);
        }
    }
}
