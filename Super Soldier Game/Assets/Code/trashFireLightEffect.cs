﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trashFireLightEffect : MonoBehaviour
{
    public UnityEngine.Experimental.Rendering.Universal.Light2D fireLight;
    // Start is called before the first frame update
    void Start()
    {
        fireLight = this.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fireLight.pointLightInnerRadius > 5)
        {
            fireLight.pointLightInnerRadius = 4;
        }
        else if (fireLight.pointLightInnerRadius < 1)
        {
            fireLight.pointLightInnerRadius = 2;
        }
        else
        {
            int randNum = Random.Range(0, 999);
            if (randNum > 500)
            {
                fireLight.pointLightInnerRadius += 0.05f;
            }
            else
            {
                fireLight.pointLightInnerRadius -= 0.05f;
            }
        }
        Color currentColor = fireLight.color;
        if (currentColor.g > 0.859)
        {
            fireLight.color = new Color(currentColor.r, 0.82f, currentColor.b, 1);
        }
        else if (currentColor.g < 0.625)
        {
            fireLight.color = new Color(currentColor.r, 0.664f, currentColor.b, 1);
        }
        else
        {
            int randNum = Random.Range(0, 999);
            if (randNum > 500)
            {
                currentColor.g += 0.01f;
                fireLight.color = currentColor;
            }
            else
            {
                currentColor.g -= 0.01f;
                fireLight.color = currentColor;
            }
        }
    }
}
