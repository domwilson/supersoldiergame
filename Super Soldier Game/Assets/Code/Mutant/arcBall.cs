﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arcBall : MonoBehaviour
{
    Vector2 start;
    Vector2 end;
    float height = 10;
    float time = 0;

    // Start is called before the first frame update
    void Start()
    {
        start = this.transform.position;
        end = GameObject.FindGameObjectWithTag("Player").transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime/3;
        this.transform.position = new Vector2(Mathf.Lerp(start.x, end.x, time), start.y + (height * Mathf.Sin(time * Mathf.PI)));
        Debug.Log("Updated postion");
    }
}
