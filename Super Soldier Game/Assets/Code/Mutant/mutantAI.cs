﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mutantAI : MonoBehaviour
{
    public float WINDUP_TIME = 1f;
    public float STUN_TIME = 2f;
    public float MOVE_SPEED = 10f;
    const float MAX_SPEED = 6f;

    CapsuleCollider2D chargeTrigger;

    Rigidbody2D enemy2D;
    Transform player;

    public bool playerIsClose = false;

    bool stunned = false;
    bool charging = false;
    bool playerToRight = false;

    float stateTime = 0f;
    float checkTime = 0f;

    public Camera mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        chargeTrigger = this.GetComponent<CapsuleCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        enemy2D = this.GetComponent<Rigidbody2D>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(playerIsClose && Time.time - stateTime > checkTime)
        {
            //Hit obstacle, stunned
            if(stunned)
            {
                //Wait X Time
                setStateTime();
                checkTime = STUN_TIME;
                stunned = false;
            }
            else
            {
                //Mutant Charging
                if(charging)
                {
                    //Set Speed, Direction
                    if (playerToRight)
                    {
                        //Move Right
                        enemy2D.AddForce(Vector2.right * MOVE_SPEED);
                    }
                    else
                    {
                        //Move Left
                        enemy2D.AddForce(Vector2.left * MOVE_SPEED);
                    }
                    if(enemy2D.velocity.magnitude >= MAX_SPEED)
                    {
                        enemy2D.velocity = new Vector2(Mathf.Sign(enemy2D.velocity.x) * MAX_SPEED, enemy2D.velocity.y);
                    }
                }
                //Windup
                else
                {
                    //Wait Y Time
                    setStateTime();
                    checkTime = WINDUP_TIME;
                    charging = true;
                    if(player.transform.position.x > this.transform.position.x)
                    {
                        playerToRight = true;
                    }
                    else
                    {
                        playerToRight = false;
                    }
                }
            }
        }
        Vector3 screenPoint = mainCamera.WorldToViewportPoint(this.transform.position);
        if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1)
        {
            playerIsClose = true;
        }
        else if (screenPoint.z < 0 && screenPoint.x < 0 && screenPoint.x > 1 && screenPoint.y < 0 && screenPoint.y > 1)
        {
            playerIsClose = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.IsTouching(chargeTrigger) && (collision.tag == "ground" || collision.tag == "Player"))
        {
            stunned = true;
            charging = false;
        }
    }

    void setStateTime()
    {
        stateTime = Time.time;
    }
}
