﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grenadeExplode : MonoBehaviour
{
    const float explodeTime = 3f;
    const float explodeForce = 1000f;
    const float maxExplodeRadius = 5f;
    const float minExplodeRadius = 1f;
    float startTime = 0;
    public GameObject explodeEffect;
    private void OnEnable()
    {
        startTime = Time.time;
    }
    // Update is called once per frame
    void Update()
    {
        if (Time.time - startTime > explodeTime)
        {
            Collider2D[] killed = Physics2D.OverlapCircleAll(this.transform.position, minExplodeRadius);
            foreach (Collider2D dead in killed)
            {
                if(dead.tag == "Player")
                {
                    //Player Dead
                    dead.transform.GetComponent<playerHealth>().killInstantly();
                }
                else if (dead.tag == "enemy" ||
                    dead.tag == "Drone" ||
                    dead.tag == "mutant")
                {
                    dead.transform.GetComponent<enemyHealth>().killInstantly();
                }
            }
            Collider2D[] damaged = Physics2D.OverlapCircleAll(this.transform.position, maxExplodeRadius);
            foreach (Collider2D hit in damaged)
            {
                if (hit.tag == "Player")
                {
                    //Player Dead
                    hit.transform.GetComponent<playerHealth>().loseHealth();
                }
                else if (hit.tag == "enemy" ||
                    hit.tag == "Drone" ||
                    hit.tag == "mutant")
                {
                    hit.transform.GetComponent<enemyHealth>().loseHealth();
                }
            }
            Instantiate(explodeEffect).transform.position = this.transform.position;
            Destroy(this.gameObject);
        }
    }
}