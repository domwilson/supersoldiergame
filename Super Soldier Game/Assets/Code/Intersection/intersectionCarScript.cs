﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class intersectionCarScript : MonoBehaviour
{
    const float finalRadius = 0.5f;
    intersectionController intersection;

    float randTime;

    public UnityEngine.Experimental.Rendering.Universal.Light2D leftLight = null;
    public UnityEngine.Experimental.Rendering.Universal.Light2D rightLight = null;
    // Start is called before the first frame update
    void Start()
    {
        intersection = this.GetComponentInParent<intersectionController>(); 
    }

    // Update is called once per frame
    void Update()
    {
        if(intersection.startIntersection)
        {
            randTime = intersection.getWaitTime();
            float deltaTime = intersection.getTime();

            //Set Size of Transform
            this.transform.localScale = new Vector3(1.75f * scalingEquation(deltaTime), 1.75f * scalingEquation(deltaTime), this.transform.lossyScale.z);

            //Set Height
            this.transform.position = new Vector3(this.transform.position.x, 4 - scalingEquation(deltaTime), this.transform.position.z);

            rightLight.pointLightInnerRadius = finalRadius * scalingEquation(deltaTime);
            leftLight.pointLightInnerRadius =  finalRadius *  scalingEquation(deltaTime);
            rightLight.pointLightOuterRadius = 2.5f * scalingEquation(deltaTime);
            leftLight.pointLightOuterRadius = 2.5f * scalingEquation(deltaTime);
            rightLight.intensity = 5 * scalingEquation(deltaTime);
            leftLight.intensity = 5 * scalingEquation(deltaTime);

            leftLight.enabled = true;
            rightLight.enabled = true;
            this.GetComponent<SpriteRenderer>().enabled = true;

        }
    }

    float scalingEquation(float time)
    {
        return (Mathf.Pow(time * (1 / intersection.getWaitTime()), 2));
    }
}
