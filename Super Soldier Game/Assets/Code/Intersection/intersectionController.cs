﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class intersectionController : MonoBehaviour
{
    const float initialWait = 2;

    BoxCollider2D box;
    CircleCollider2D trigger;

    Camera mainCamera;

    public float randomTime;
    float startTime;

    float min = 3f;
    float max = 4;

    public bool startIntersection = false;
    bool firstCompleted = false;


    // Start is called before the first frame update
    void Start()
    {
        box = this.GetComponent<BoxCollider2D>();
        trigger = this.GetComponent<CircleCollider2D>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(box.enabled)
        {
            box.enabled = false;
        }
        if (startIntersection)
        {
            if (!firstCompleted)
            {
                if (Time.time - startTime > initialWait)
                {
                    box.enabled = true;
                    newRandomNum();
                    startTime = Time.time;
                    firstCompleted = true;
                }
            }
            else
            {
                if (Time.time - startTime > randomTime)
                {
                    box.enabled = true;
                    newRandomNum();
                    startTime = Time.time;
                }
            }
        }
        else
        {
            Vector3 screenPoint = mainCamera.WorldToViewportPoint(this.transform.position);
            if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1)
            {
                startIntersection = true;
                startTime = Time.time;
            }
        }
    }

    void newRandomNum()
    {
        randomTime = Random.Range(min, max);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            startIntersection = true;
            startTime = Time.time;
        }
    }

    public float getTime()
    {
        return Time.time - startTime;
    }

    public float getWaitTime()
    {
        if (!firstCompleted)
        {
            return initialWait;
        }
        else
        {
            return randomTime;
        }
    }
}
