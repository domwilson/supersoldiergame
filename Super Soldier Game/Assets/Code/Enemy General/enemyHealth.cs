﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyHealth : MonoBehaviour
{
    public int enemyLives = 1;
    public int currentHealth;
    public bool killable = false;
    public float scoreMultiplier = 1.0f;
    public bool beenKilled;

    public Behaviour[] obj;
    public Vector3 start;
    public playerScorer scorer;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = enemyLives;
        obj = this.GetComponents<Behaviour>();
        start = this.transform.position;
        scorer = GameObject.Find("character").GetComponent<playerScorer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(currentHealth <= 0)
        {
            //Die
            if(!beenKilled)
            {
                scorer.enemyDead(scoreMultiplier);
                scorer.addKilledEnemy(this.gameObject);
                this.transform.position = start;
                this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                beenKilled = true;
            }
            this.GetComponent<SpriteRenderer>().enabled = false;
            for (int i = 0; i < obj.Length; i++)
            {
                if (obj[i] != this)
                {
                    obj[i].enabled = false;
                }
            }
            //Destroy(this.gameObject);
        }
    }

    public void loseHealth()
    {
        currentHealth--;
    }

    public void killInstantly()
    {
        for (int i = 0; i <= enemyLives; i++)
        {
            loseHealth();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!killable)
        {
            if (collision.collider.gameObject.layer == LayerMask.NameToLayer("PProjectile"))
            {
                loseHealth();
            }
        }
        if (collision.collider.tag == "instantDeath")
        {
            killInstantly();

        }
    }

    public void unDie()
    {
        beenKilled = false;
        currentHealth = enemyLives;
        this.GetComponent<SpriteRenderer>().enabled = true;
        this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        for (int i = 0; i < obj.Length; i++)
        {
            if (obj[i] != this)
            {
                obj[i].enabled = true;
            }
        }
        if (this.TryGetComponent(typeof(gruntAI), out Component component))
        {
            this.GetComponent<gruntAI>().idle = true;
        }
        else if (this.TryGetComponent(typeof(mutantAI), out Component component1))
        {
            this.GetComponent<mutantAI>().playerIsClose = false;
        }
    }
}
