﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class levelTimer : MonoBehaviour
{
    private float levelStartTime;
    private float levelEndTime;

    TextMeshProUGUI timerDisplay;
    // Start is called before the first frame update
    void Start()
    {
        levelStartTime = Time.time;
        timerDisplay = this.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        timerDisplay.SetText(Mathf.Round(Time.time - levelStartTime).ToString());
    }
}
