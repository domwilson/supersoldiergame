﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine;

public class startGame : MonoBehaviour
{
    public TMP_Dropdown dropdown;
    public string difficultyMode;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseUp()
    {
        difficultyMode = dropdown.options[dropdown.value].text;
        int newAdHealth = 0;
        float newAdRPM = 5;
        if (difficultyMode == "Very Easy")
        {
            newAdHealth = 0;
            newAdRPM = Random.Range(4, 5);
        }
        if (difficultyMode == "Easy")
        {
            newAdHealth = 0;
            newAdRPM = Random.Range(3, 4);
        }
        if (difficultyMode == "Medium")
        {
            newAdHealth = 1;
            newAdRPM = Random.Range(3, 4);
        }
        if (difficultyMode == "Hard")
        {
            newAdHealth = 2;
            newAdRPM = Random.Range(2, 3);
        }
        if (difficultyMode == "Very Hard")
        {
            newAdHealth = 3;
            newAdRPM = Random.Range(1, 2);
        }
        PlayerPrefs.SetInt("newAdHealth", newAdHealth);
        PlayerPrefs.SetFloat("newAdRPM", newAdRPM);
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
}
