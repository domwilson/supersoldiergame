﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class medkitSpawner : MonoBehaviour
{

    public GameObject medkit;
    bool canSpawn = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void instantiateMedkit()
    {
        if(canSpawn)
        {
            Instantiate(medkit, this.transform).transform.position = this.transform.position;
            canSpawn = false;
        }
        
    }
}
