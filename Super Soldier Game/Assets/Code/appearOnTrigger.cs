﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class appearOnTrigger : MonoBehaviour
{
    public GameObject triggerEnemy;


    Behaviour[] obj;
    public bool triggered = false;
    // Start is called before the first frame update
    void Start()
    {
        obj = this.GetComponents<Behaviour>();
    }

    // Update is called once per frame
    void Update()

    {
        if (triggerEnemy.GetComponent<Collider2D>().enabled == false && !triggerEnemy.TryGetComponent(typeof(appearOnTrigger), out Component component2))
        {
            triggered = true;
        }
        else if (triggerEnemy.TryGetComponent(typeof(gruntAI), out Component component))
        {
            if (!triggerEnemy.GetComponent<gruntAI>().idle)
            {
                triggered = true;
            }
        }
        else if (triggerEnemy.TryGetComponent(typeof(mutantAI), out Component component1))
        {
            if (triggerEnemy.GetComponent<mutantAI>().playerIsClose)
            {
                triggered = true;
            }
        }
        if (triggered)
        {
            this.GetComponent<SpriteRenderer>().enabled = true;
            this.GetComponent<Rigidbody2D>().gravityScale = 3;
            this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            for (int i = 0; i < obj.Length; i++)
            {
                obj[i].enabled = true;
            }
            this.GetComponent<appearOnTrigger>().enabled = false;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().enabled = false;
            for (int i = 0; i < obj.Length; i++)
            {
                if(obj[i] != this.GetComponent<appearOnTrigger>())
                {
                    obj[i].enabled = false;
                }

            }
        }
    }
}