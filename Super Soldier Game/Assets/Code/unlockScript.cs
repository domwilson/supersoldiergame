﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unlockScript : MonoBehaviour
{
    public string unlockedAbility = "";
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt(unlockedAbility, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Player")
        {
            PlayerPrefs.SetInt(unlockedAbility, 1);
            Debug.Log("Set Int");
            Destroy(this.gameObject);
            
        }
    }
}
