﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class checkpointLogic : MonoBehaviour
{
    public int medkitSpawnPercentage = 1;
    public List<Transform> zoneSegments = new List<Transform>();
    Transform player;
    int currentPlayerZone = 0;
    public scoreSystem scorer;

    bool spawnMedkit = false;

    int oldZone;
    // Start is called before the first frame update
    void Start()
    {
        zoneSegments = GetComponentsInChildren<Transform>().OfType<Transform>().ToList();
        zoneSegments.RemoveAt(0);
        player = GameObject.Find("character").GetComponent<Transform>();
        scorer = GameObject.Find("scoreDisplay").GetComponent<scoreSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        updatePosition();
        if(oldZone < currentPlayerZone)
        {
            //Reset list
            player.GetComponent<playerScorer>().resetList();
            scorer.passCheckpoint();
        }
        oldZone = currentPlayerZone;
        if(spawnMedkit)
        {
            int randDigit = Random.Range(0, 100);
            if(randDigit < medkitSpawnPercentage)
            {
                zoneSegments[currentPlayerZone + 1].GetComponent<medkitSpawner>().instantiateMedkit();
            }
            else
            {
                zoneSegments[currentPlayerZone + 2].GetComponent<medkitSpawner>().instantiateMedkit();
            }
            
            spawnMedkit = false;
        }
    }

    void updatePosition()
    {
        for(int i = 0; i < zoneSegments.Count; i++)
        {
            if((player.position.x > zoneSegments[i].position.x && player.position.x < zoneSegments[i + 1].position.x))
            {
                currentPlayerZone = i;
            }
        }
    }

    public void onPlayerHit()
    {
        spawnMedkit = true;
    }

    public Vector3 getLastCheckpointPosition()
    {
        return zoneSegments[currentPlayerZone].position;
    }

    public int getCurrentZone()
    {
        return currentPlayerZone;
    }
}
