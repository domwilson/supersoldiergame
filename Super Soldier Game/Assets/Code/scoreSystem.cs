﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class scoreSystem : MonoBehaviour
{
    //This script tracks the players score and broadcasts it to the score board.

    TextMeshProUGUI scoreDisplay;
    TextMeshProUGUI multiplierDisplay;


    Color32 secMultiplier = new Color32(0,191,193, 255);
    Color32 thirdMultiplier = new Color32(0, 193, 60, 255);
    Color32 fourthMultiplier = new Color32(202, 209, 0, 255);
    Color32 fifthMultiplier = new Color32(209, 140, 0, 255);
    Color32 sixthMultiplier = new Color32(209, 24, 0, 255);
    public int zoneScore = 0;
    public int currentScore = 0;
    int displayScore = 0;
    public int scoreMultiplier = 1;

    const int firstCutoff = 100;
    const int secondCutoff = 599;

    // Start is called before the first frame update
    void Start()
    {
        scoreDisplay = this.GetComponent<TextMeshProUGUI>();
        multiplierDisplay = GameObject.Find("multiplierDisplay").GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(getSpeedMultiplier());
        if(currentScore + zoneScore > displayScore)
        {
            if(currentScore + zoneScore - displayScore < firstCutoff)
            {
                displayScore+= 2;
            }
            else if (currentScore + zoneScore - displayScore < secondCutoff)
            {
                displayScore += 11;
            }
            else if (currentScore + zoneScore - displayScore > secondCutoff)
            {
                displayScore += 111;
            }
        }
        else if (currentScore + zoneScore < displayScore)
        {
            displayScore = currentScore + zoneScore;
        }
        string scoreVal;
        if (displayScore > 999999)
        {
            scoreVal = 999999.ToString();
        }
        else
        {
            scoreVal = displayScore.ToString();
        }
        for(int i = 6 - scoreVal.Length; i > 0; i--)
        {
            scoreVal = scoreVal.Insert(0, "0");
        }
        scoreDisplay.SetText("Score: " + scoreVal);

        if (scoreMultiplier > 1)
        {
            multiplierDisplay.SetText(scoreMultiplier + "x");
        }
        else
        {
            multiplierDisplay.SetText("");
        }

        Color32 newColor = secMultiplier;
        if (scoreMultiplier == 3)
        {
            newColor = thirdMultiplier;
        }
        else if (scoreMultiplier == 4)
        {
            newColor = fourthMultiplier;
        }
        else if (scoreMultiplier == 5)
        {
            newColor = fifthMultiplier;
        }
        else if (scoreMultiplier == 6)
        {
            newColor = sixthMultiplier;
        }
        multiplierDisplay.faceColor = newColor;

    }

    public void addScore(int score)
    {
        zoneScore += score;
    }

    public void reduceScoreByPercentage(float percentage)
    {
        currentScore = Mathf.RoundToInt(((float)currentScore) * (1f - percentage));
    }

    public float getSpeedMultiplier()
    {
        return ((float)scoreMultiplier/6f) + 1f;
    }

    public void passCheckpoint()
    {
        currentScore += zoneScore;
        zoneScore = 0;
    }

    public void onRespawn()
    {
        zoneScore = 0;
        reduceScoreByPercentage(0.1f);
    }
}
