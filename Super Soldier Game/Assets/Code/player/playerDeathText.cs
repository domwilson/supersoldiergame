﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerDeathText : MonoBehaviour
{
    public GameObject deathScreen;
    public playerHealth playerLife;
    // Start is called before the first frame update
    void Start()
    {
        deathScreen.SetActive(false);
        playerLife = GameObject.FindGameObjectWithTag("Player").GetComponent<playerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
           if (playerLife.currentHealth <= 0)
        {
            deathScreen.SetActive(true);
        }
        else
        {
            deathScreen.SetActive(false);
        }
    }
}
