﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerHud : MonoBehaviour
{
    public playerHealth health;
    public GameObject playerLife;
    //public GameObject deathScreen;
    public GameObject[] iconArray = new GameObject[playerHealth.playerLives];
    int iconCount;
    // Start is called before the first frame update
    void Start()
    {
        //deathScreen.GetComponent<Canvas>.enabled = false;
        for (int i = playerHealth.playerLives; i > 0; i--)
        {
            GameObject life = Instantiate(playerLife, this.transform);
            life.GetComponent<RectTransform>().anchoredPosition = new Vector2(-400 + (50 * i), -200);
            iconArray[i - 1] = life;
        }
        iconCount = playerHealth.playerLives;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void deleteIcon()
    {
        Destroy(iconArray[iconCount - 1]);
        iconCount--;
    }

    public void addIcon()
    {
        GameObject life = Instantiate(playerLife, this.transform);
        iconCount++;
        life.GetComponent<RectTransform>().anchoredPosition = new Vector2(-400 + (50 * (iconCount)), -200);
        iconArray[iconCount - 1] = life;

    }
}
