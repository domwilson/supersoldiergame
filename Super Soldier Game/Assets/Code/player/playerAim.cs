﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAim : MonoBehaviour
{
    public enum Direction { Up, Left, Right, UpLeft, DownLeft, UpRight, DownRight, None };
    //0.92387...
    const double num1 = 0.9238795325;
    //0.3826834.....
    const double num2 = 0.3826834324;
    // Start is called before the first frame update
    void Start()
    {

    }

    public static Vector2 GetDirection(float xDirection, float yDirection)
    {
        Vector2 aimAxis = new Vector2(xDirection, yDirection);
        aimAxis = aimAxis.normalized;
        return aimAxis;
    }

    public static float getAngle(float xDirection, float yDirection)
    {
        if (xDirection < 0)
        {
            return (Mathf.Atan2(xDirection, yDirection)) * Mathf.Rad2Deg * Mathf.Sign(xDirection);
        }
        else
        {
            return (Mathf.Atan2(xDirection, yDirection)) * Mathf.Rad2Deg * -Mathf.Sign(xDirection);
        }
    }
}
