﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerProjectile : MonoBehaviour
{
    bool playerCanSee = false;

    public UnityEngine.Experimental.Rendering.Universal.Light2D flash;
    float spawnTime;
    // Start is called before the first frame update
    void Awake()
    {
        spawnTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        flash.intensity = flashScale();
        if(GetComponent<Renderer>().isVisible || playerCanSee)
        {
            playerCanSee = true;
            if (!GetComponent<Renderer>().isVisible)
            {
                Destroy(this.gameObject);
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.gameObject.layer == LayerMask.NameToLayer("Floor") ||
           collision.collider.gameObject.layer == LayerMask.NameToLayer("Enemy") ||
           collision.collider.gameObject.layer == LayerMask.NameToLayer("Drone"))
        {
            Destroy(this.gameObject);
        }
    }

    float flashScale()
    {
        if(1 / (10f * (Time.time - spawnTime)) > 10)
        {
            return 10;
        }
        return 1 / (10f * (Time.time - spawnTime));
    }
}
