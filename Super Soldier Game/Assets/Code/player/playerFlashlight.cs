﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerFlashlight : MonoBehaviour
{
    public UnityEngine.Experimental.Rendering.Universal.Light2D flashlight;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float angle = 0;
        if(playerAim.GetDirection(getAimX(), getAimY()) == Vector2.zero)
        {
            Vector2 aimDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            aimDirection = aimDirection - new Vector2(this.transform.position.x, this.transform.position.y);
            aimDirection.Normalize();
            angle = playerAim.getAngle(aimDirection.x, aimDirection.y);
        }
        else
        {
            angle = playerAim.getAngle(getAimX(), getAimY());
        }
        this.transform.rotation = Quaternion.Euler(0, 0, angle);
    }
    static float getAimX()
    {
        return Input.GetAxis("AimX");
    }

    static float getAimY()
    {
        return Input.GetAxis("AimY");
    }
}
