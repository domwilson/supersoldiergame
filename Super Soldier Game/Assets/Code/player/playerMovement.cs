﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    public const float jumpFactor = 9f;
    public const float holdFactor = 1.875f;

    public const float moveFactor = 10f;
    public const float maxSpeed = 6f;
    public const float slowRate = -5f;

    public const float dashMagnitude = 15f;
    public const float dashWait = 2.5f;
    public const float invulnerableWait = 1f;

    public const float moveWait = 0.25f;

    public const float climbSpeed = 5f;

    public Rigidbody2D player2D;

    public scoreSystem scorer;

    public bool forceJump = false;

    bool onGround = false;
    float direction = 0;
    bool faceRight = true;

    bool canJump = false;
    bool releasedButton = false;
    float jumpTime = 0;
    bool hasDoubleJumped = false;
    float secondJumpFactor = 0.375f;

    public bool canDash = true;

    bool isDashing = false;
    bool isInvulnerable = false;
    float dashTime = 0f;

    bool moving = false;
    float moveTime = 0f;
    int prevRight = 0;

    bool isClimbing = false;

    bool isOnFlag = false;
    float flagTime = 0f;
    bool alreadyScoredFlag = false;

    public bool angleCameraDown = false;
    float timeSinceLastSlope = 0;

    public Cinemachine.CinemachineVirtualCamera slopeCam;

    //Unlocks
    bool canDoubleJump = false;


    // Start is called before the first frame update
    void Start()
    {
        player2D = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    //Processes player input into character commands
    void FixedUpdate()
    {
        player2D.constraints = RigidbodyConstraints2D.FreezeRotation;
        if (!isClimbing && !isOnFlag)
        {
            checkUnlocks();
            processMove();
            processJump();
            processCrouch();

            if (canDash)
            {
                processDash();
            }

            processDirection();
            processCameraPos();
        }
        else
        {
            if(isClimbing)
            {
                processClimb();
            }
            else if (isOnFlag)
            {
                processFlag();
            }
        }
    }

    void checkUnlocks()
    {
        if (!canDoubleJump && PlayerPrefs.GetInt("doubleJump") == 1)
        {
            canDoubleJump = true;
            Debug.Log("UnlockedDoubleJump");
        }
    }
    //Processes MoveX axis, translates it into character speed 
    //Handles slowing down normally & edge cases (Dashing)
    void processMove()
    {
        player2D.AddForce(new Vector2(moveFactor * getMoveX(), 0));

        float sign = Mathf.Sign(player2D.velocity.x);
        if (Mathf.Abs(player2D.velocity.x) > maxSpeed * scorer.getSpeedMultiplier() && !isDashing)
        {
          //  if(onGround)
           // {
                player2D.velocity = new Vector2(sign * maxSpeed * scorer.getSpeedMultiplier(), player2D.velocity.y);
           // }
            //else
            //{
                //player2D.velocity = new Vector2(sign * maxSpeed * 1.25f, player2D.velocity.y);
            //}
        }

        else if (Mathf.Abs(player2D.velocity.x) > maxSpeed && isDashing)
        {
            player2D.AddForce(new Vector2(sign * 3 * slowRate, 0));
        }

        else if (Mathf.Abs(player2D.velocity.x) > 0.1 && getMoveX() == 0)
        {
            player2D.AddForce(new Vector2(sign * slowRate, 0));
        }
        else if (getMoveX() == 0 || getMoveX() > 0 && player2D.velocity.x < 0 || getMoveX() < 0 && player2D.velocity.x > 0)
        {
            player2D.velocity = new Vector2(0, player2D.velocity.y);
        }

        updateDirection();
    }

    //Keeps track of player direction, useful for animation
    public void updateDirection()
    {
        if (player2D.velocity.x > 0)
        {
            direction = 1;
            faceRight = true;
        }
        else if (player2D.velocity.x < 0)
        {
            direction = -1;
            faceRight = false;
        }
        else
        {
            direction = 0;
        }
    }

    /* 
     * Handles Jumping
     * Checking if player can jump
     * Handling longer jumping for holding button longer.
    */
    void processJump()
    {
        //Check if on ground and able to jump
        if (onGround && !canJump)
        {
            canJump = true;
            releasedButton = false;
            hasDoubleJumped = false;
        }
        //If player input says so, jump.
        else if ((canJump && Input.GetAxis("Jump") > 0) || forceJump)
        {
            player2D.AddForce(Vector2.up * jumpFactor, ForceMode2D.Impulse);
            onGround = false;
            canJump = false;
            jumpTime = Time.time;
            forceJump = false;
        }
        //Add force for holding button down while jumping
        else if (!canJump && Input.GetAxis("Jump") > 0 && player2D.velocity.y > 0 && !releasedButton)
        {
            if (!hasDoubleJumped)
            {
                player2D.AddForce(Vector2.up * jumpFunction() * (scorer.getSpeedMultiplier() / 2), ForceMode2D.Impulse);
            }
            else
            {
                player2D.AddForce(Vector2.up * secondJumpFactor * jumpFunction(), ForceMode2D.Impulse);
            }

        }
        //After releasing button, cannot rehold it to jump farther.
        //TODO Fix this b/c 0 is not when button is released, could cause issues.
        else if (!canJump && Input.GetAxis("Jump") == 0)
        {
            releasedButton = true;
        }
        else if (!hasDoubleJumped && releasedButton && canDoubleJump && Input.GetAxis("Jump") > 0)
        {
            canJump = true;
            hasDoubleJumped = true;
            releasedButton = false;
        }
    }
    /*
     * Controls the addition of force to the jump using a linear mx+b function. 
     * holdFactor * 0.5 ensures jump will last less than half a second, then start falling
     */
    float jumpFunction()
    {
        return (-holdFactor * (Time.time - jumpTime)) + (holdFactor * (0.5f));
    }

    void processCrouch()
    {
        if (getMoveY() < 0)
        {
            //Player Crouches
        }
    }

    /*
    * Roll Dash
    * Quick Burst in Movement Direction (X Axis) 
    * Remove EProjectile collisions, cannot be harmed during roll
    * Renable at end
    */

    void processDash()
    {
        if (((prevRight == 1 && getMoveX() > 0) || (prevRight == -1 && getMoveX() < 0)) && Time.time - moveTime < moveWait && !isDashing)
        {
            player2D.velocity = Vector2.zero;
            //TODO add directional control to dash?
            //Vector2 direction = playerAim.translateDirection(getMoveX(), getMoveY())
            player2D.AddForce(new Vector2(Mathf.Sign(getMoveX()) * dashMagnitude, 0), ForceMode2D.Impulse);
            isDashing = true;
            isInvulnerable = true;
            dashTime = Time.time;
            prevRight = 0;
        }
        else if (Time.time - dashTime > dashWait)
        {
            isDashing = false;
        }

        else if (Time.time - dashTime > invulnerableWait)
        {
            isInvulnerable = false;
        }

        if (Mathf.Abs(getMoveX()) > 0 && Mathf.Abs(getMoveY()) < 0.75f && !isDashing)
        {
            moving = true;
            if (getMoveX() > 0)
            {
                prevRight = 1;
            }
            else
            {
                prevRight = -1;
            }
        }

        else if (getMoveX() == 0 && Mathf.Abs(getMoveY()) < 0.75f && moving && !isDashing)
        {
            moveTime = Time.time;
            moving = false;
        }

        if (isInvulnerable)
        {
            Physics2D.IgnoreLayerCollision(8, 11, true);
            Physics2D.IgnoreLayerCollision(8, 15, true);
        }
        else
        {
            Physics2D.IgnoreLayerCollision(8, 11, false);
            Physics2D.IgnoreLayerCollision(8, 15, false);
        }

    }

    void processDirection()
    {
        if (faceRight)
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    void processCameraPos()
    {
        if(Time.time - timeSinceLastSlope < 2)
        {
            angleCameraDown = true;
        }
        else
        {
            angleCameraDown = false;
        }
    }

    void processClimb()
    {
        if (getMoveX() > 0)
        {
            player2D.velocity = Vector2.up * climbSpeed;
        }
        else if (getMoveX() < 0)
        {
            player2D.velocity = Vector2.down * climbSpeed;
        }
        else
        {
            player2D.velocity = Vector2.zero;
        }
    }

    void processFlag()
    {
        if (Time.time - flagTime > 1.5)
        {
            player2D.gravityScale = 1;
            player2D.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            if (onGround == true)
            {
                //Disable flag collider
                GameObject flagPole = GameObject.FindGameObjectWithTag("flagPole");
                flagPole.GetComponent<BoxCollider2D>().enabled = false;
                player2D.constraints = RigidbodyConstraints2D.FreezeRotation;
                player2D.velocity = (Vector2.right * 4);
            }
        }
        else
        {
            if(!alreadyScoredFlag)
            {
                this.GetComponent<playerScorer>().addPointsBasedOnFlag(this.transform.position.y);
                alreadyScoredFlag = true;
            }
            player2D.gravityScale = 0;
            player2D.velocity = Vector2.zero;
            player2D.constraints = RigidbodyConstraints2D.FreezeAll;
            GameObject.Find("FlagCam").GetComponent<Cinemachine.CinemachineVirtualCamera>().enabled = true;

        }

    }

    float scaleMovement(float xAxis)
    {
        return (Mathf.Pow(xAxis, 2) * Mathf.Sign(xAxis));
    }
    //return true if right, false if left
    public float getDirection()
    {
        return direction;
    }

    public bool getFaceRight()
    {
        return faceRight;
    }
    //NEgative for left, Positive for Right
    float getMoveX()
    {
        return Input.GetAxis("MoveX");
    }
    //Positive for Jump, Negative for Crouch
    float getMoveY()
    {
        return Input.GetAxis("MoveY");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "flagPole")
        {
            isOnFlag = true;
            flagTime = Time.time;
            this.GetComponent<playerScorer>().stopKillStreak();
        }
        if(collision.transform.tag == "ground" && Time.time > 10)
        {
            slopeCam.enabled = false;
            GameObject.Find("Blender").transform.position = new Vector3(this.transform.position.x + 30, this.transform.position.y, -315.0199f);
        }
    }
    void OnCollisionStay2D(Collision2D col)
    {
        if ((col.transform.tag == "ground" || col.transform.tag == "shootablePlatform") && Input.GetAxis("Jump") == 0)
        {
            onGround = true;
        }
        else if (col.transform.tag == "slope")
        {
            slopeCam.enabled = true;
            timeSinceLastSlope = Time.time;
            if (Input.GetAxis("Jump") == 0)
            {
                onGround = true;
            }
            player2D.AddForce(-2 * Physics2D.gravity);
            if(Input.GetAxis("MoveX") == 0)
            {
                player2D.constraints = RigidbodyConstraints2D.FreezeAll;
            }
            else
            {
                player2D.constraints = RigidbodyConstraints2D.FreezeRotation;
            }
        }
        else if (col.transform.tag == "climbableWall")
        {
            isClimbing = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "climbableWall")
        {
            isClimbing = false;
        }
    }
}
