﻿using UnityEngine;

public class playerShoot : MonoBehaviour
{
    static float projectileSpeed = 500;
    public GameObject projectile;
    public int fireMode = 0;
    public bool canBurst = false;
    public bool canAuto = false;

    bool canFire = true;
    double fireTime = 0;
    double resetSpeed = 0.5;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        checkCanFire();

        if (Input.GetAxis("Fire") > 0 && canFire)
        {
            Vector2 aimDirection;
            //Check if there is no mouse movement
            if (Input.GetAxis("Mouse X") == 0 && Input.GetAxis("Mouse Y") == 0 &&
                (getAimX() != 0 || getAimY() != 0))
            {
                aimDirection = playerAim.GetDirection(getAimX(), getAimY());
                //Set Animation to Direction
            }

            //If there is mouse movement, use that for aim
            else
            {
                aimDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                aimDirection = aimDirection - new Vector2(this.transform.position.x, this.transform.position.y);
                aimDirection.Normalize();
            }
            /*            else
                        {
                                if (this.GetComponent<playerMovement>().getFaceRight())
                                {
                                    aimDirection = new Vector2(1, 0);
                                }
                                else
                                {
                                    aimDirection = new Vector2(-1, 0);
                                }
                        }*/
            GameObject newProjectile = createProjectile();
            newProjectile.GetComponent<Rigidbody2D>().AddForce((projectileSpeed * aimDirection));
            canFire = false;
            fireTime = Time.time;
        }
    }
    void checkCanFire()
    {
        if (fireMode == 0)
        {
            if (Input.GetAxis("Fire") == 0
                && Time.time - fireTime > resetSpeed)
            {
                canFire = true;
            }
        }
        else if (fireMode < 0)
        {
            if (!canFire && Time.time - fireTime > resetSpeed)
            {
                canFire = true;
            }
        }
    }
    GameObject createProjectile()
    {
        GameObject newProjectile = Instantiate(projectile);
        newProjectile.transform.position = this.transform.position;
        return newProjectile;
    }

    static float getAimX()
    {
        return Input.GetAxis("AimX");
    }

    static float getAimY()
    {
        return Input.GetAxis("AimY");
    }
}
