﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerGrenadeThrow : MonoBehaviour
{
    public const float maxThrowForce = 20f;
    public const float maxTime = 2f;
    public bool canGrenade = false;
    public GameObject grenade;

    bool willThrowGrenade = false;

    float launchTime;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (canGrenade)
        {
            if (Input.GetAxis("Grenade") > 0 && !willThrowGrenade)
            {
                willThrowGrenade = true;
                launchTime = Time.time;
            }
            if (Input.GetAxis("Grenade") == 0 && willThrowGrenade)
            {
                GameObject newGrenade = Instantiate(grenade);
                newGrenade.transform.position = this.transform.position;
                newGrenade.GetComponent<Rigidbody2D>().AddForce(forceFunction() * maxThrowForce * playerAim.GetDirection(getAimX(), getAimY()), ForceMode2D.Impulse);
                willThrowGrenade = false;
            }
        }
    }

    float forceFunction()
    {
        float returnVal = (Time.time - launchTime) / maxTime;
        if(returnVal > 1)
        {
            returnVal = 1;
        }
        Debug.Log(returnVal);
        return returnVal;
    }

    static float getAimX()
    {
        return Input.GetAxis("AimX");
    }

    static float getAimY()
    {
        return Input.GetAxis("AimY");
    }

}
