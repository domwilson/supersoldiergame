﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScorer : MonoBehaviour
{
    //This script is attached to the player and controls any and all player score events and processes them adds them to the scoreSystem.
    public scoreSystem score;
    const float MULTIPLIERTIME = 4;
    const float INCREMENTTIME = 1;
    const int enemyDeath = 100;

    float killTime = 0;
    float tickTime = 0;
    bool decreasingMultiplier = false;

    public List<GameObject> killedEnemies = new List<GameObject>();

    // Update is called once per frame
    void Update()
    {
        if (Time.time - killTime > MULTIPLIERTIME && !decreasingMultiplier && score.scoreMultiplier > 1)
        {
            score.scoreMultiplier--;
            tickTime = Time.time;
            decreasingMultiplier = true;
        }
        else if (decreasingMultiplier && Time.time - tickTime > INCREMENTTIME && score.scoreMultiplier > 1)
        {
            score.scoreMultiplier--;
            tickTime = Time.time;
            decreasingMultiplier = true;
        }
    }

    public void enemyDead(float enemyMultiplier)
    {
        score.addScore((int)(enemyDeath * enemyMultiplier * score.scoreMultiplier));
        if (Time.time - killTime < MULTIPLIERTIME && score.scoreMultiplier < 6)
        {
            score.scoreMultiplier++;
        }
        killTime = Time.time;
        decreasingMultiplier = false;
    }

    public void stopKillStreak()
    {
        score.scoreMultiplier = 1;
    }

    public void addPointsBasedOnFlag(float playerY)
    {
        score.addScore((int)((playerY - 3) * 1000));
    }

    public void addKilledEnemy(GameObject enemy)
    {
        killedEnemies.Add(enemy);
    }

    public void resetList()
    {
        for (int i = 0; i < killedEnemies.Count; i++)
        {
            Destroy(killedEnemies[i]);
        }
        killedEnemies.Clear();
    }

    public void respawnList()
    {
        for (int i = 0; i < killedEnemies.Count; i++)
        {
            //Reset all attribues;
            killedEnemies[i].GetComponent<enemyHealth>().unDie();
        }
        killedEnemies.Clear();
        Debug.Log("Enemies Reset");
    }
}
