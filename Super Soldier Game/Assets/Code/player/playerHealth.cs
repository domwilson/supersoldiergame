﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerHealth : MonoBehaviour
{
    public const int playerLives = 3;
    public const float invulnTime = 2;
    public const float flickerTime = 2;

    public int currentHealth;
    public Behaviour[] obj;
    public playerHud hud;
    public checkpointLogic checkpoints;
    public SpriteRenderer render;
    public Canvas deathScreen;

    float hitTime = 0;

    bool dead = false;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = playerLives;
        hud = GameObject.FindGameObjectWithTag("MainCamera").transform.Find("Canvas").GetComponent<playerHud>();
        checkpoints = GameObject.Find("checkpoints").GetComponent<checkpointLogic>();
        obj = this.GetComponents<Behaviour>();
        render = this.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 0 && !dead)
        {
            die();
            dead = true;
        }
        if (Time.time - hitTime < flickerTime && hitTime > 0)
        {
            //Mess with sprite renderer alpha value.
            render.color = Color.Lerp(new Color(1, 0.5f, 0.5f, 0.25f), Color.white, 3 * Mathf.PingPong(Time.time, 0.3333333f));
        }
        else
        {
            render.color = Color.white;
        }
    }

    //Go Commit Die
    public void die()
    {
        render.enabled = false;
        transform.Find("Point Light 2D").gameObject.SetActive(false);
       // GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>().followPlayer = false;
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        for (int i = 0; i < obj.Length; i++)
        {
            if (obj[i] != this && obj[i] != this.GetComponent<playerScorer>())
            {
                obj[i].enabled = false;

            }
        }
        deathScreen.enabled = true;
    }

    //Go Commit UnDie
    public void unDie()
    {
        render.enabled = true;
        transform.Find("Point Light 2D").gameObject.SetActive(true);
        //GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraControl>().followPlayer = true;
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        for (int i = 0; i < obj.Length; i++)
        {
            if (obj[i] != this)
            {
                obj[i].enabled = true;
            }
        }
        addHealth(3);
        dead = false;
        Debug.Log("Successfully commited undie");
    }

    public void loseHealth()
    {
        if (Time.time - hitTime > invulnTime)
        {
            currentHealth--;
            hud.deleteIcon();
            checkpoints.onPlayerHit();
            hitTime = Time.time;
            this.GetComponent<playerScorer>().stopKillStreak();
        }
    }

    public void killInstantly()
    {
        int j = currentHealth;
        for (int i = 0; i < j; i++)
        {
            currentHealth--;
            hud.deleteIcon();
            this.GetComponent<playerScorer>().stopKillStreak();
        }
    }
    public void addHealth(int addedHealth)
    {
        if (currentHealth + addedHealth <= 3)
        {
            for (int i = 0; i < addedHealth; i++)
            {
                currentHealth++;
                hud.addIcon();
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("EProjectile") ||
            collision.collider.tag == "mutant")
        {
            loseHealth();
        }

        if (collision.collider.tag == "instantDeath")
        {
            killInstantly();
        }
    }
}
