﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gruntAI : MonoBehaviour
{
    Rigidbody2D enemy2D;
    
    public bool idle = true;
    Transform player;
    public const float idealDistance = 17.5f;

    public const float moveSpeed = 3f;

    public float triggerRadius = 20f;
    CircleCollider2D playerTrigger;

    public bool canShoot = false;

    public bool pace = false;
    bool paceLeft = true;
    public float paceDistance = 1f;
    float startingLocation;
    CapsuleCollider2D paceCollider;

    public bool moveTowardsPlayer = true;

    Camera mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        enemy2D = this.GetComponent<Rigidbody2D>();
        startingLocation = this.transform.position.x;
        paceCollider = this.GetComponent<CapsuleCollider2D>();
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (idle)
        {
            if(pace)
            {
                if(paceLeft)
                {
                    //Pace Left
                    enemy2D.velocity = (new Vector2(-1, enemy2D.velocity.y));
                }
                else
                {
                    //Pace Right
                    enemy2D.velocity = (new Vector2(1, enemy2D.velocity.y));
                }
                if(this.transform.position.x > startingLocation + paceDistance)
                {
                    paceLeft = true;
                }
                else if (this.transform.position.x < startingLocation - paceDistance)
                {
                    paceLeft = false;
                }
            }
        }
        else
        {
            if(moveTowardsPlayer)
            {
                float difference = player.position.x - this.transform.position.x;
                if (Mathf.Abs(difference) > idealDistance)
                {
                    if (difference > 0)
                    {
                        enemy2D.velocity = (new Vector2(moveSpeed, enemy2D.velocity.y));
                    }
                    else
                    {
                        enemy2D.velocity = (new Vector2(-moveSpeed, enemy2D.velocity.y));
                    }
                }
                else
                {
                    enemy2D.velocity = Vector2.zero;
                }
                
            }
        }
        Vector3 screenPoint = mainCamera.WorldToViewportPoint(this.transform.position);
        if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1 
            && Mathf.Abs(player.transform.position.x - this.transform.position.x) < 15)
        {
            idle = false;
            canShoot = true;
        }
        else if (screenPoint.z < 0 && screenPoint.x < 0 && screenPoint.x > 1 && screenPoint.y < 0 && screenPoint.y > 1
            && Mathf.Abs(player.transform.position.x - this.transform.position.x) > 15)
        {
            idle = true;
            canShoot = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.IsTouching(paceCollider) && collision.tag != "Player" || collision.tag != "enemy")
        {
            paceLeft = !paceLeft;
        }
    }
}
