﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gruntShootAI : MonoBehaviour
{
    public double resetSpeed = 4;
    Transform player;
    gruntAI movementAI;
    enemyAim EnemyAim;
    public GameObject projectile;
    bool canFire = true;
    float fireTime = 0f;

    public bool targetPlayer = false;
    // Start is called before the first frame update
    void Start()
    {
        movementAI = this.GetComponent<gruntAI>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        EnemyAim = this.GetComponent<enemyAim>();
        resetSpeed = Random.Range(3.5f, 4.5f);
    }

    // Update is called once per frame
    void Update()
    {
        checkCanFire();
        if(movementAI.canShoot && canFire)
        {
            GameObject newProjectile = Instantiate(projectile);
            newProjectile.transform.position = this.transform.position;
            if (targetPlayer)
            {
                Vector2 aimDirection = player.position;
                aimDirection = aimDirection - new Vector2(this.transform.position.x, this.transform.position.y);
                newProjectile.GetComponent<Rigidbody2D>().AddForce(aimDirection * 4, ForceMode2D.Impulse);
            }
            else
            {
                newProjectile.GetComponent<Rigidbody2D>().AddForce(EnemyAim.translateDirection() * 4, ForceMode2D.Impulse);
            }
            canFire = false;
            fireTime = Time.time;
        }
    }
    void checkCanFire()
    {
        if (!canFire && Time.time - fireTime > resetSpeed)
        {
            canFire = true;
        }
    }
}
