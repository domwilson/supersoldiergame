﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mBossShootAI : MonoBehaviour
{
    public float smallThrowResetSpeed = 3;
    Transform player;
    mutantAI movementAI;
    enemyAim EnemyAim;
    public GameObject projectile;
    bool canFire = true;
    float fireTime = 0f;
    public float projectileSpeed = 1.5f;
    bool canSmallThrow = true;
    int smallThrowCounter = 0;
    float bigThrowResetSpeed = 4;
    int arcThrowCounter = 0;
    public GameObject bigProjectile;

    // Start is called before the first frame update
    void Start()
    {
        movementAI = this.GetComponent<mutantAI>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        EnemyAim = this.GetComponent<enemyAim>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canSmallThrow)
        {
            checkCanFire(smallThrowResetSpeed);
            if (movementAI.playerIsClose && canFire)
            {
                smallThrow();
                smallThrowCounter++;
            }
            if (smallThrowCounter > 2)
            {
                canSmallThrow = false;
                smallThrowCounter = 0;
            }
        }
        else
        {
            checkCanFire(bigThrowResetSpeed);
            if (movementAI.playerIsClose && canFire)
            {
                arcThrow();
                arcThrowCounter++;
            }
            if (arcThrowCounter > 1)
            {
                canSmallThrow = true;
                arcThrowCounter = 0;
            }
        }
    }

    void smallThrow()
    {
        GameObject newProjectile = Instantiate(projectile);
        newProjectile.transform.position = this.transform.position;
        Vector2 aimDirection = player.position + Vector3.up + (Vector3.up * (Mathf.Sign(Random.Range(-1, 1))));
        aimDirection = aimDirection - new Vector2(this.transform.position.x, this.transform.position.y);
        aimDirection.Normalize();
        newProjectile.GetComponent<Rigidbody2D>().AddForce(aimDirection * projectileSpeed, ForceMode2D.Impulse);
        canFire = false;
        fireTime = Time.time;
    }

    void arcThrow()
    {
        GameObject newProjectile = Instantiate(bigProjectile);
        newProjectile.transform.position = this.transform.position;
        Debug.Log("Made Ball");
        canFire = false;
        fireTime = Time.time;
    }

    void checkCanFire(float resetSpeed)
    {
        if (!canFire && Time.time - fireTime > resetSpeed)
        {
            canFire = true;
        }
    }
}
