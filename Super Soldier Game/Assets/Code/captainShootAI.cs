﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class captainShootAI : MonoBehaviour
{
    public double resetSpeed = 4;
    Transform player;
    gruntAI movementAI;
    enemyAim EnemyAim;
    public GameObject projectile;
    bool canFire = true;
    float fireTime = 0f;
    // Start is called before the first frame update
    void Start()
    {
        movementAI = this.GetComponent<gruntAI>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        EnemyAim = this.GetComponent<enemyAim>();
        resetSpeed = Random.Range(3.5f, 4.5f);
    }

    // Update is called once per frame
    void Update()
    {
        checkCanFire();
        if(movementAI.canShoot && canFire)
        {
            for (int i = -1; i <= 1; i++) {
                GameObject newProjectile = Instantiate(projectile);
                newProjectile.transform.position = this.transform.position;
                newProjectile.GetComponent<Rigidbody2D>().AddForce(EnemyAim.translateDirection() * 4 + (Vector2.up * i), ForceMode2D.Impulse);
            }
            canFire = false;
            fireTime = Time.time;
        }
    }
    void checkCanFire()
    {
        if (!canFire && Time.time - fireTime > resetSpeed)
        {
            canFire = true;
        }
    }
}
