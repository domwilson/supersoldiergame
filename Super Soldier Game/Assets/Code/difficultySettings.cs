﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class difficultySettings : MonoBehaviour
{
    public string difficultyMode;
    private int newAdHealth;
    private double newAdRPM;

    // Start is called before the first frame update
    void Start()
    {
        if (difficultyMode == "Very Easy")
        {
            newAdHealth = 1;
            newAdRPM = Random.Range(4, 5);
        }
        if (difficultyMode == "Easy")
        {
            newAdHealth = 1;
            newAdRPM = Random.Range(3, 4);
        }
        if (difficultyMode == "Medium")
        {
            newAdHealth = 1;
            newAdRPM = Random.Range(3, 4);
        }
        if (difficultyMode == "Hard")
        {
            newAdHealth = 2;
            newAdRPM = Random.Range(2, 3);
        }
        if (difficultyMode == "Very Hard")
        {
            newAdHealth = 3;
            newAdRPM = Random.Range(1, 2);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
