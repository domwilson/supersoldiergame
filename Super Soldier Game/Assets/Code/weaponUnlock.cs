﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weaponUnlock : MonoBehaviour
{
    public string weaponName;
    bool unlock = false;

    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if(unlock)
        {
            if(weaponName == "grenade")
            {
                player.GetComponent<playerGrenadeThrow>().canGrenade = true;
            }
            else if (weaponName == "burstFire")
            {
                player.GetComponent<playerShoot>().canBurst = true;
            }
            else if (weaponName == "autoFire")
            {
                player.GetComponent<playerShoot>().canAuto = true;
            }
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            unlock = true;

        }
    }
}
