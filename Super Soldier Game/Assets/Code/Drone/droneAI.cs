﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class droneAI : MonoBehaviour
{
    public GameObject projectile;
    Rigidbody2D enemy2D;

    Transform player;

    public const float moveSpeed = 3f;

    public float triggerRadius = 20f;
    CircleCollider2D playerTrigger;

    public bool playerIsClose = false;

    bool paceLeft = true;
    public float paceDistance;
    public float shootRange = 0.25f;

    public const double resetSpeed = 4;
    bool canFire = true;
    float fireTime = 0f;

    float randomOffset;

    Camera mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        playerTrigger = this.GetComponent<CircleCollider2D>();
        playerTrigger.radius = triggerRadius;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        enemy2D = this.GetComponent<Rigidbody2D>();
        randomOffset = Random.Range(-1, 1);
        paceDistance = Random.Range(1, 3);
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(playerIsClose)
        {
            if (paceLeft)
            {
                //Pace Left
                enemy2D.velocity = (new Vector2(-moveSpeed, enemy2D.velocity.y));
            }
            else
            {
                //Pace Right
                enemy2D.velocity = (new Vector2(moveSpeed, enemy2D.velocity.y));
            }
            if (this.transform.position.x > player.position.x + randomOffset + paceDistance)
            {
                paceLeft = true;
            }
            else if (this.transform.position.x < player.position.x + randomOffset - paceDistance)
            {
                paceLeft = false;
            }
            checkCanFire();
            if(this.transform.position.x < player.position.x + shootRange && this.transform.position.x > player.transform.position.x - shootRange && canFire)
            {
                GameObject newProjectile = Instantiate(projectile);
                newProjectile.transform.position = this.transform.position;
                newProjectile.GetComponent<Rigidbody2D>().AddForce(Vector2.down * 5, ForceMode2D.Impulse);
                canFire = false;
                fireTime = Time.time;
            }

        }
        Vector3 screenPoint = mainCamera.WorldToViewportPoint(this.transform.position);
        if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1)
        {
            playerIsClose = true;
        }
        else if (screenPoint.z < 0 && screenPoint.x < 0 && screenPoint.x > 1 && screenPoint.y < 0 && screenPoint.y > 1)
        {
            playerIsClose = false;
        }
    }
    void checkCanFire()
    {
        if (!canFire && Time.time - fireTime > resetSpeed)
        {
            canFire = true;
        }
    }
}
