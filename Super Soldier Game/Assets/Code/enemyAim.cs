﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAim : MonoBehaviour
{
    public enum Direction {Up, Left, Right, UpLeft, DownLeft, UpRight, DownRight, Down, None};

    Transform player;
    Transform enemy;

    //0.92387...
    float num1;
    //0.3826834.....
    float num2;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        enemy = this.transform;
        num1 = Mathf.Cos(Mathf.Deg2Rad * 22.5f);
        num2 = Mathf.Sin(Mathf.Deg2Rad * 22.5f);
    }

    public Direction GetDirection()
    {
        Direction returnVal = Direction.None;
        Vector3 transform = player.position - enemy.position;
        transform = transform.normalized;

        Vector2 transformAngle = new Vector2(transform.x, transform.y);

        if(transformAngle.x > -num2 && transformAngle.x < num2 && transformAngle.y >= num1)
        {
            returnVal = Direction.Up;
        }
        else if (transformAngle.y > -num2 && transformAngle.y < num2 && transformAngle.x >= num1)
        {
            returnVal = Direction.Right;
        }
        else if (transformAngle.y > -num2 && transformAngle.y < num2 && transformAngle.x <= num1)
        {
            returnVal = Direction.Left;
        }
        else if (transformAngle.y < num1 && transformAngle.y > num2 && transformAngle.x >= num2)
        {
            returnVal = Direction.UpRight;
        }
        else if (transformAngle.y < num1 && transformAngle.y > num2 && transformAngle.x <= -num2)
        {
            returnVal = Direction.UpLeft;
        }
        else if (transformAngle.y > -num1 && transformAngle.y < -num2 && transformAngle.x <= -num2)
        {
            returnVal = Direction.DownLeft;
        }
        else if (transformAngle.y > -num1 && transformAngle.y < -num2 && transformAngle.x <= num1)
        {
            returnVal = Direction.DownRight;
        }
        else if (transformAngle.x > -num2 && transformAngle.x < num2 && transformAngle.y < -num1)
        {
            returnVal = Direction.Down;
        }
        return returnVal;
    }

    public Vector2 translateDirection()
    {
        Direction direction = GetDirection();
        Vector2 returnVector = Vector2.zero;
        if(direction == Direction.Up)
        {
            returnVector = Vector2.up;
        }
        else if (direction == Direction.Right)
        {
            returnVector = Vector2.right;
        }
        else if (direction == Direction.Left)
        {
            returnVector = Vector2.left;
        }
        else if (direction == Direction.UpRight)
        {
            returnVector = new Vector2 (1,1);
        }
        else if (direction == Direction.UpLeft)
        {
            returnVector = new Vector2(-1, 1);
        }
        else if (direction == Direction.DownRight)
        {
            returnVector = new Vector2(1, -1);
        }
        else if (direction == Direction.DownLeft)
        {
            returnVector = new Vector2(-1, -1);
        }
        else
        {
            returnVector = new Vector2(0, -1);
        }
        
        return returnVector.normalized;
    }
}
