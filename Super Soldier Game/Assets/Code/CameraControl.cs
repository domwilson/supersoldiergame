﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CameraControl : MonoBehaviour
{
    playerMovement player;
    public CinemachineVirtualCamera cam;


    public bool onSlope = false;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<playerMovement>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        onSlope = false;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "slope")
        {
            onSlope = true;
        }
    }
}


