﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class respawnPlayer : MonoBehaviour
{
    public checkpointLogic checkpoints;
    public playerHealth health;
    public scoreSystem scorer;
    public Transform player;

    public List<GameObject> allEnemies;
    // Start is called before the first frame update
    void Start()
    {
        checkpoints = GameObject.Find("checkpoints").GetComponent<checkpointLogic>();
        player = GameObject.Find("character").transform;
        health = player.GetComponent<playerHealth>();
        scorer = GameObject.Find("scoreDisplay").GetComponent<scoreSystem>();
    }

    public void onPress()
    {
        respawn();
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.Joystick1Button1) && this.transform.parent.GetComponent<Canvas>().enabled)
        {
            allEnemies.Clear();
            respawn();
        }
    }

    private void respawn()
    {
        this.transform.parent.GetComponent<Canvas>().enabled = false;
        player.GetComponent<playerScorer>().respawnList();
        scorer.onRespawn();
        player.transform.position = checkpoints.getLastCheckpointPosition();
        player.GetComponent<playerHealth>().unDie();
        GameObject[] grunts = GameObject.FindGameObjectsWithTag("enemy");
        GameObject[] mutants = GameObject.FindGameObjectsWithTag("mutant");
        for (int i = 0; i < grunts.Length; i++)
        {
            allEnemies.Add(grunts[i]);
        }
        for (int j = 0; j < mutants.Length; j++)
        {
            allEnemies.Add(mutants[j]);
        }
        for (int j = 0; j < allEnemies.Count; j++)
        {
            if (allEnemies[j].TryGetComponent(typeof(gruntAI), out Component component))
            {
                allEnemies[j].GetComponent<gruntAI>().idle = true;
                allEnemies[j].GetComponent<gruntAI>().canShoot = false;
            }
            if (allEnemies[j].TryGetComponent(typeof(droneAI), out Component component4))
            {
                allEnemies[j].GetComponent<droneAI>().playerIsClose = false;
            }
            if (allEnemies[j].TryGetComponent(typeof(mutantAI), out Component component1))
            {
                allEnemies[j].GetComponent<mutantAI>().playerIsClose = false;
            }
            if (allEnemies[j].TryGetComponent(typeof(appearOnTrigger), out Component component2))
            {
                allEnemies[j].GetComponent<appearOnTrigger>().enabled = true;
                allEnemies[j].GetComponent<appearOnTrigger>().triggered = false;
                allEnemies[j].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                
            }
            allEnemies[j].transform.position = allEnemies[j].GetComponent<enemyHealth>().start;
        }
        Debug.Log("Player haz been respawned");
    }
}
